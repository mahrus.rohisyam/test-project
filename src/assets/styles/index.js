export const color = {
  primary: '#06D6A0',
  secondary: '#00383D',
  white: '#fff',
  dark: '#1e1e1e',
  grey: '#c4c4c4',
};
