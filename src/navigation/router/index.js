import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Home, Splash, Preview} from '../../screens';

// const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
const Hide = {headerShown: false};

// const MainApp = () => {
//   return (
//     <Tab.Navigator tabBar={props => <BottomNav {...props} />}>
//       <Tab.Screen name="Home" component={Home} options={Hide} />
//     </Tab.Navigator>
//   );
// };

const index = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Home" component={Home} options={Hide} />
        <Stack.Screen name="Splash" component={Splash} options={Hide} />
        <Stack.Screen name="Preview" component={Preview} options={Hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default index;
