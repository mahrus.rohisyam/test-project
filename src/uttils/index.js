const heightMobileUI = 896;
const widthMobileUI = 414;
const API_KEY = '6bf98907136689ab59ea553c335dbdca'
const BASE_URL = 'https://www.themoviedb.org'
import {Dimensions} from 'react-native';

export const responsiveWidth = width => {
  return (Dimensions.get('window').width * width) / widthMobileUI;
};

export const responsiveHeight = height => {
  return (Dimensions.get('window').height * height) / heightMobileUI;
};

export{API_KEY, BASE_URL}