import React, {Component} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native';
import {color} from '../../assets';
import {CText} from '../../components';

export const IMG_URL = 'https://image.tmdb.org/t/p/w500';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.route.params.data,
    };
  }

  render() {
    const {data} = this.state;
    const {id} = this.props.route.params;
    return (
      <View style={{flex: 1}}>
        {id == 1 ? (
          <View style={styles.mainContainer}>
            <Image
              source={{uri: data.Poster}}
              style={{widht: '100%', height: '75%'}}
              resizeMode="cover"
            />
            <View style={{margin: 10}}>
              <CText style={styles.title}>{data.Title}</CText>
              <CText style={styles.subtitle}>{data.Genre}</CText>
              <CText style={styles.desc}>{data.Plot}</CText>
            </View>
          </View>
        ) : (
          <View style={styles.mainContainer}>
            <Image
              source={{uri: IMG_URL + data.poster_path}}
              style={{width: '100%', height: '75%'}}
              resizeMode="cover"
            />
            <View style={{margin: 10}}>
              <CText style={styles.title}>{data.title}</CText>
              <CText style={styles.subtitle}>{data.release_data}</CText>
              <CText style={styles.desc}>{data.overview}</CText>
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: color.dark,
  },
  title: {
    fontSize: 25,
  },
  subtitle: {
    color: color.grey,
  },
  desc: {
    marginVertical: 10,
  },
});
