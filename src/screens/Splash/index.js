import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class index extends Component {
    componentDidMount(){
        const {navigation} = this.props
        setTimeout(() => {
            navigation.replace('Home')
        }, 2000);
    }

    render() {
        return (
            <View style={styles.mainView}>
                <Text style={styles.text}> Splash </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView:{
        alignItems:'center',
        justifyContent:"center",
        flex:1
    }
})
