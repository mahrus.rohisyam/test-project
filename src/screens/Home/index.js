import React, {Component} from 'react';
import {
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import axios from 'axios';
import {
  API_KEY,
  BASE_URL,
  responsiveHeight,
  responsiveWidth,
} from '../../uttils';
import {color, Logo, Notif} from '../../assets';
import {Card, CText} from '../../components';

export const TREND_URL = 'https://api.themoviedb.org/3';
export const FEAT_URL = 'https://www.omdbapi.com';
export const IMG_URL = 'https://image.tmdb.org/t/p/w500';
export class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trend: null,
      featured: null,
    };
  }

  async componentDidMount() {
    this._getFeat();
    this._getTrending();
  }

  _getFeat = async () => {
    const featured = await axios.get(`${FEAT_URL}`, {
      params: {
        apikey: '132789a7',
        t: 'Rambo',
        y: '2020',
      },
    });

    this.setState({
      featured: featured.data,
    });
  };

  _getTrending = async () => {
    const trend = await axios.get(`${TREND_URL}/trending/movie/day`, {
      params: {
        api_key: API_KEY,
      },
    });

    this.setState({
      trend: trend.data.results,
    });
  };

  render() {
    const {trend, featured} = this.state;
    const {navigation} = this.props;

    return (
      <View style={styles.mainView}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 25,
          }}>
          <Image source={Logo} style={styles.logo} />
          <TouchableOpacity style={{marginLeft: '20%'}}>
            <Image source={Notif} style={styles.notificationIcon} />
          </TouchableOpacity>
        </View>

        <ScrollView scrollEventThrottle={16}>
          <View style={{marginHorizontal: '5%'}}>
            {featured && (
              <Pressable
                onPress={() => {
                  navigation.navigate('Preview', {data: featured, id: 1});
                }}>
                <Image
                  source={{uri: featured.Poster}}
                  style={styles.poster}
                  resizeMode="cover"
                />
                <View style={styles.textOverlap}>
                  <CText style={styles.title}>
                    {featured.Title} ({featured.Year})
                  </CText>
                </View>
              </Pressable>
            )}

            <CText style={{fontSize: responsiveWidth(16), marginTop: 20}}>
              Trending
            </CText>

            {trend && (
              <View style={{marginBottom: 10}}>
                <ScrollView horizontal={true}>
                  {trend.map((val, i) => {
                    return (
                      <Card
                        onPress={() => {
                          navigation.navigate('Preview', {data: val, id: 2});
                        }}
                        key={i}
                        title={val.title}
                        uri={`${IMG_URL}${val.poster_path}`}
                        genre={val.genre}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default index;
const styles = StyleSheet.create({
  mainView: {
    backgroundColor: color.dark,
    flex: 1,
  },
  logo: {
    width: responsiveWidth(90),
    height: responsiveHeight(35),
    marginLeft: '40%',
  },
  notificationIcon: {
    width: responsiveWidth(50),
    height: responsiveHeight(60),
  },
  poster: {
    width: '100%',
    height: responsiveHeight(500),
    borderRadius: 20,
    opacity: 0.8,
  },
  title: {
    fontSize: responsiveWidth(20),
  },
  subtitle: {
    fontSize: responsiveWidth(20),
    color: '#c4c4c4',
  },
  textOverlap: {
    position: 'absolute',
    bottom: 20,
    left: 20,
  },
});
