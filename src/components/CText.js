import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {color} from '../assets';
import {responsiveWidth} from '../uttils';

export default class CText extends Component {
  render() {
    const {style} = this.props;
    return (
      <Text style={{...styles.text, ...style}}>{this.props.children}</Text>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    color: color.white,
    fontSize: responsiveWidth(14),
    fontFamily: 'FredokaOne-Regular',
  },
});
