import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, Pressable} from 'react-native';
import {CText} from '.';
import {color} from '../assets';
import {responsiveHeight, responsiveWidth} from '../uttils';

export default class Card extends Component {
  render() {
    return (
      <View style={{margin: 10}}>
        <Pressable onPress={this.props.onPress} style={styles.mainContainer}>
          <Image
            source={{
              uri: this.props.uri,
            }}
            resizeMode="cover"
            style={styles.poster}
          />
          <View style={{marginVertical: 10}}>
            <CText style={styles.title}>{this.props.title}</CText>
            <CText style={styles.subtitle}>{this.props.genre}</CText>
          </View>
        </Pressable>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    marginVertical: 20,
    width: responsiveWidth(130),
    height: responsiveHeight(250),
  },
  subtitle: {
    color: color.grey,
  },
  poster: {
    width: '100%',
    height: responsiveHeight(200),
    borderRadius: 25,
  },
});
