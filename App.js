import React from 'react'
import { View, Text } from 'react-native'
import {Router} from './src/navigation'

const App = () => {
  return (
    <Router/>
  )
}

export default App
